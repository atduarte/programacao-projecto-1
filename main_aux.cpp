#include <string>
#include <vector>
#include <sstream> // stringstreams...
#include <algorithm> //random_shuffle....
#include <ctime>
#include <cstdlib>
#include <iomanip>

const int ID_LENGTH = 7;
const int NOME_LENGTH = 20;
const int SALDO_LENGTH = 13;
const int CASA_LENGTH = 2;
const int APOSTA_LENGTH = 22;

/* Outros auxiliares */
template <class T>
string toString( T argument)
{
       string r;
       stringstream s;

       s << argument;
       r = s.str();

       return r;
}

vector<string> explode(const string &delimiter, const string &str)
{
    vector<string> arr;

    int strleng = str.length();
    int delleng = delimiter.length();
    if (delleng==0)
        return arr;//no change

    int i=0; 
    int k=0;
    while( i<strleng )
    {
        int j=0;
        while (i+j<strleng && j<delleng && str[i+j]==delimiter[j])
            j++;
        if (j==delleng)//found delimiter
        {
            
            arr.push_back(  str.substr(k, i-k) );    
            i+=delleng;
            k=i;
        }
        else
        {
            i++;
        }
    }

    arr.push_back(  str.substr(k, i-k) );

    return arr;
}

bool isInteger(const std::string &s)
{
   	if(s.empty() || ((!isdigit(s[0])) && (s[0] != '-') && (s[0] != '+')))
   		return false;

  	char * p ;
   	strtol(s.c_str(), &p, 10) ;
  	return true ;
}

bool isFloat(const string &myString ) {
    std::istringstream iss(myString);
    float f;
    iss >> noskipws >> f; // noskipws considers leading whitespace invalid
    // Check the entire string was consumed and if either failbit or badbit is set
    return iss.eof() && !iss.fail(); 
}

/* Forms */
string id_form(int id) {
	string m_id = toString(id);
	int id_len = m_id.length();
	for(int i = 0; i<(ID_LENGTH - id_len); i++) {
		m_id.insert(0, "0");
	}
	return m_id;
}
string nome_form(string nome) {
	string m_nome = nome;
	int nome_len = m_nome.length();
	for(int i = 0; i<(NOME_LENGTH - nome_len); i++) {
		m_nome += " ";
	}
	return m_nome;
}
string saldo_form(double saldo) {
	saldo = floorf(saldo * 100 + 0.5) / 100;
	string m_saldo = toString(saldo);
	int saldo_len = m_saldo.length();
	for(int i = 0; i<SALDO_LENGTH - saldo_len; i++) {
		m_saldo.insert(0, " ");
	}
	return m_saldo;
}
string casa_form(double casa) {
	string m_casa = toString(casa);
	int casa_len = m_casa.length();
	for(int i = 0; i<CASA_LENGTH - casa_len; i++) {
		m_casa.insert(0, " ");
	}
	return m_casa;
}

/* Função devolve vector de n numeros inteiros aleatorios entre dois valores e sem repetição */
vector<int> randomize(int min, int max, int n) {
	vector<int> vec_random;
	for(int i = min; i<=max; i++) {
		vec_random.push_back(i);
	}
	random_shuffle(vec_random.begin(), vec_random.end());
	vec_random.erase (vec_random.begin()+n, vec_random.end());
	sort(vec_random.begin(),vec_random.end());

	return vec_random;
}
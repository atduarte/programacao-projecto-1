#include <fstream>
#include <iostream>

using namespace std;

#include "main_aux.cpp"
#include "players_aux.cpp" // ler player.txt; Ir buscar nome e alterar saldo
#include "bets_aux.cpp" 
#include "prize_key_aux.cpp" 
#include "winners_aux.cpp"

int aposta_n_acertos_casas, aposta_n_acertos_estrelas;
string aposta_comparada, buffer_line;
bool prize_key_executed = false;

int main() {

	cout << "== Atribuicao dos Premios ==" << endl << endl;
	cout << "Tem a certeza que pretende atribuir os premios? (Y/N) ";
	
	string validate = "n";
	getline(cin,validate);

	//Verificar se existem a duas linhas no ficheiro prize_key.txt
	//Basicamente: Se o Bets e o Prize_key correram
	ifstream prize_key_testing ("prize_key.txt");

	if(prize_key_testing) {
		getline(prize_key_testing,buffer_line);
		getline(prize_key_testing,buffer_line);
		if(buffer_line.size() > 5)
			prize_key_executed = true;
	}
	
	prize_key_testing.close();

	//Verificar dados recebido, se o Prize_Key foi executado e executar o programa
	if((validate == "Y" || validate == "y") && prize_key_executed) {

		//Ler e colocar Players no vector jogadores
		readPlayers();

		//Ler e colocar Prize_key no vector main_kf
		readPrize_key();

		//Definir vector premios conforme as regras
		reset_premios();

		// Passamos as infos dos jogadores (id e nome) e m_premio_total = 0
		for(int i = 0; i < jogadores.size(); i++) {
			jogadores_winners.push_back(make_info_jogador_vencedor(jogadores[i].m_id, jogadores[i].m_nome));
			jogadores_winners[i].m_premio_total = 0;
		}

		//Ler e colocar Apostas no vector jogadores_winners.m_apostas
		readBets();

		//Para cada jogador
		for(int i = 0; i < jogadores_winners.size(); i++)
		{   // Para cada aposta
			for(int j = 0; j < jogadores_winners[i].m_apostas.size(); j++) 
			{	
				aposta_comparada = "";
				aposta_n_acertos_casas = 0;
				aposta_n_acertos_estrelas = 0;

				// CASAS CASAS CASAS CASAS 
				//Para cada casa da aposta do jogador
				for(int k = 0; k < 5; k++)
				{	//Para cada casa da aposta da main_kf
					for(int l = 0; l < 5; l++)
					{   //Caso seja igual
						if(jogadores_winners[i].m_apostas[j].m_casas[k] == main_kf.m_casas[l]) {
							aposta_comparada += casa_form(main_kf.m_casas[l]);
							aposta_comparada += "*";
							aposta_comparada += " ";
							aposta_n_acertos_casas++;
							break;
						} else if (l == 4){
							aposta_comparada += casa_form(jogadores_winners[i].m_apostas[j].m_casas[k]);
							aposta_comparada += " ";
							aposta_comparada += " ";
						}
					}
				} 

				//Separar casas das estrelas na aposta
				aposta_comparada += "- ";

				// *****
				//Para cada estrela da aposta do jogador
				for(int k = 0; k < 2; k++)
				{	//Para cada estrela da aposta da main_kf
					for(int l = 0; l < 2; l++)
					{	//
						if(jogadores_winners[i].m_apostas[j].m_estrelas[k] == main_kf.m_estrelas[l]) {
							aposta_comparada += casa_form(main_kf.m_estrelas[l]);
							aposta_comparada += "*";
							aposta_comparada += " ";
							aposta_n_acertos_estrelas++;
							break;
						} else if (l == 1) {
							aposta_comparada += casa_form(jogadores_winners[i].m_apostas[j].m_estrelas[k]);
							aposta_comparada += " ";
							aposta_comparada += " ";
						}
					}
				} 			

				//Verifica se tem prémio
				//Caso tenha adiciona ao vector m_apostas_ganhas
				if(aposta_n_acertos_casas == 5 && aposta_n_acertos_estrelas >= 0 ||
				   aposta_n_acertos_casas == 4 && aposta_n_acertos_estrelas >= 0 ||
				   aposta_n_acertos_casas == 3 && aposta_n_acertos_estrelas >= 0 ||
				   aposta_n_acertos_casas == 2 && aposta_n_acertos_estrelas >= 0 ||
				   aposta_n_acertos_casas == 1 && aposta_n_acertos_estrelas == 2
				) {
					info_aposta_vencedora temp_info;
					temp_info.m_aposta_com_asterisco = aposta_comparada;
					temp_info.m_acertos_casas = aposta_n_acertos_casas;
					temp_info.m_acertos_estrelas = aposta_n_acertos_estrelas;

					jogadores_winners[i].m_apostas_ganhas.push_back(temp_info);

				} 

				//Adicionar ao numvec
				if(devolve_n_premio(aposta_n_acertos_casas,aposta_n_acertos_estrelas) > 0) {
					premios[devolve_n_premio(aposta_n_acertos_casas,aposta_n_acertos_estrelas)-1].m_numvenc++;
				} 
			}

			if(jogadores_winners[i].m_apostas_ganhas.size() == 0) {
				jogadores_winners.erase(jogadores_winners.begin()+i);
				i--;
			}
		}

		//Contar tipos de prémios que têm pelo menos um vencedor
		int count_premios = 0;
		for(int i = 0; i < premios.size(); i++) {
			if(premios[i].m_numvenc > 0)
				count_premios++;
		}

		//Calcular valor de cada premio
		double valor_cada_premio = count_bets()*1.0/count_premios;

		for(int i = 0; i < premios.size(); i++) {
			if(premios[i].m_numvenc > 0)
				premios[i].m_valor = valor_cada_premio*1.0/premios[i].m_numvenc;
		}

		int pos_premio;

		//Para cada jogador
		for(int i = 0; i < jogadores_winners.size(); i++)
		{	//Inicializa premio total a 0
			jogadores_winners[i].m_premio_total = 0.0;
		    // Para cada aposta ganha
			for(int j = 0; j < jogadores_winners[i].m_apostas_ganhas.size(); j++) 
			{	
				//Preencher no vector apostas_ganhas a coluna premio
				pos_premio = devolve_n_premio(jogadores_winners[i].m_apostas_ganhas[j].m_acertos_casas,
								 			  jogadores_winners[i].m_apostas_ganhas[j].m_acertos_estrelas)
							 -1;
				//Caso retorne um prémio [verifacação potencialmente desnecessária] acrescentar valor
				if(pos_premio > 0) 
					jogadores_winners[i].m_apostas_ganhas[j].m_premio = premios[pos_premio].m_valor;
				
				//Incrementa ao premio total do jogador
				jogadores_winners[i].m_premio_total +=  jogadores_winners[i].m_apostas_ganhas[j].m_premio;
			}

			//Actualizar Saldo
			for (int j = 0; j < jogadores.size(); j++) {
				if(jogadores_winners[i].m_id == jogadores[j].m_id){
					jogadores[j].m_saldo += jogadores_winners[i].m_premio_total;
					break;
				}
			}
		}

		//Escreve o ficheiro players.txt - Devido à modificação do Saldo
		writePlayers();
		//Escreve parte do ficheiro prize_key.txt
		writeWinnersPrize_key();
		//Escreve o ficheiro winners.txt
		writeWinners();

		cout << "Programa executado com sucesso." << endl << endl;
	} else if(!prize_key_executed) {
		cout << "Execute primeiro o Bets e o Prize_key." << endl;
	} else {
		cout << "Programa nao executado com sucesso." << endl;
	}

	system("pause");

	return 0;
}
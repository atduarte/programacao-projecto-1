#include <fstream>
#include <iostream>

using namespace std;

#include "main_aux.cpp"
#include "bets_aux.cpp"
#include "prize_key_aux.cpp"

bool bets_executed = false;

/* == Fun��es == */
void main () {
	system("cls");
	
	srand(time(NULL));

	//Cabe�alhos e pedidos
	cout << "== Sorteio da chave do concurso ==" << endl << endl;
	cout << "Tem a certeza que pretende sortear a chave? (Y/N) ";
	
	//Receber dados
	string validate = "n", buffer_line;
	getline(cin,validate);

	//Verificar se existe a primeira linha
	//Basicamente: Se o Bets correu
	ifstream bets_testing ("prize_key.txt");

	if(bets_testing) {
		getline(bets_testing,buffer_line);
		if(buffer_line.size() > 5)
			bets_executed = true;
	}
	
	bets_testing.close();


	//Verificar dados recebido e executar
	if(validate == "Y" || validate == "y") {
		if(!bets_executed)
			cout << "Devera' executar primeiro o programa bets." << endl;
		generate_writePrize_key();
		cout << "Chave gerada com sucesso." << endl;
	} else {
		cout << "Chave nao gerada com sucesso." << endl;
	}
	
	system("pause");
}


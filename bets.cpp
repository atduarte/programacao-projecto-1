#include <fstream>
#include <iostream>

using namespace std;

#include "main_aux.cpp"
#include "players_aux.cpp" // ler players.txt, verificar ID e Saldo
#include "bets_aux.cpp"

string buffer;
char confirmacao;

int main() {
	
	readPlayers();
	
	srand(time(NULL));
	
	string another_operation, another_try; //Não tem saldo. Trocar numero de apostas.
	
	do {
		system("cls");

		another_operation = 'n';
		
		int id_a_procurar, jogador_pos, n_apostas, n_apostas_auto, n_apostas_man;
		bool user_found = false;
		
		//Receber dados - ID
		cout << "== Programa de Registo de Apostas ==" << endl << endl
			 << "Introduza o seu ID de Jogador : ";
		getline(cin, buffer);

		//Verificar dados recebidos
		if(isInteger(buffer)) {
			//Converter o id para inteiro
			id_a_procurar = atoi (buffer.c_str());

			//Procurar no vector jogadores
			for(int i = 0; i < jogadores.size(); i++) {
				if(jogadores[i].m_id == id_a_procurar) {
					user_found = true;
					jogador_pos = i;
					break;
				}
			}
		}
		
		
		//Jogador encontrado no ficheiro players.txt
		if(user_found) {
			do {
				system("cls");
				another_try = 'n';
				
				//Receber dados - Número de apostas
				cout << "== Programa de Registo de Apostas == " << endl << endl;
				cout <<  "Ola, " << jogadores[jogador_pos].m_nome << endl
					 << "Quantas apostas automaticas pretende fazer? ";
				getline(cin, buffer);

				//Verificar dados recebidos
				if(isInteger(buffer)) 
					n_apostas_auto = atoi (buffer.c_str());
				else
					n_apostas_auto = 0;

				//Receber dados - Número de apostas manuais
				cout << "E quantas apostas manuais? ";
				getline(cin, buffer);

				//Verificar dados recebidos
				if(isInteger(buffer))
					n_apostas_man = atoi (buffer.c_str());
				else
					n_apostas_man = 0;

				//Somar numero de apostas
				n_apostas = n_apostas_auto + n_apostas_man;
				
				//Se tiver saldo
				if(n_apostas <= jogadores[jogador_pos].m_saldo) {
					novas_apostas.m_id = jogadores[jogador_pos].m_id;
					novas_apostas.m_apostas.clear();
					
					//Apostas Automáticas
					for(int i = 0; i < n_apostas_auto; i++) {
						novas_apostas.m_apostas.push_back (make_aposta(randomize(1,50,5), randomize(1,11,2)));
					}
					
					// Apostas Manuais
					if(n_apostas_man > 0) {
						cout << "Introduza as apostas que pretende fazer [exemplo: 1 2 5 14 50 - 5 9]: " << endl;

						for(int i = 0; i < n_apostas_man; i++) {
							cout << i+1 << ": ";

							string line_buffer;

							getline(cin,line_buffer);

							aposta aposta_buffer = stringToAposta(line_buffer);

							if(verify_aposta(aposta_buffer)) {
								novas_apostas.m_apostas.push_back(aposta_buffer);
							} else {
								cout << "Chave incorrectamente introduzida. Tente novamente." << endl;
								i--;
							}
						}
					}

					if ( n_apostas > 0 ) {
						//Pedir confirmação
						cout << endl << "Apostas a registar:" << endl;

						for (int i = 0; i < novas_apostas.m_apostas.size(); i++) {
							cout << aposta_form(novas_apostas.m_apostas[i]) << endl;
						}

						cout << "Pretende registar? (Y/N) ";
						getline(cin, buffer);

						confirmacao = buffer[0];

						if(confirmacao == 'Y' || confirmacao == 'y') {
							//Registar apostas no ficheiro
							writeBets();

							//Retirar o Saldo ao jogador
							jogadores[jogador_pos].m_saldo -= novas_apostas.m_apostas.size();
							writePlayers();
						}
					}
					
					cout << endl << "Quer fazer mais apostas com este jogador? (Y/N) ";
					getline(cin,another_try);
					another_operation = "Y"; // Aparecer tela inicial para mudar de jogador
					
				//Se não tiver saldo
				} else {
					cout << "Nao tem saldo disponivel para fazer tantas apostas" << endl
						 << "Quer tentar novamente com este jogador? (Y/N) ";
					getline(cin,another_try);
					another_operation = "Y"; // Aparecer tela inicial para mudar de jogador
				}
			} while(another_try == "Y" || another_try == "y");
			
		//Jogador não encontrado no ficheiro players.txt		
		} else {
			cout << "Utilizador nao encontrado." << endl
				 << "Quer tentar novamente? (Y/N) ";
			getline(cin,another_operation);
		}
	} while (another_operation == "Y" || another_operation == "y");
		
	system("pause");
	return 0;
}
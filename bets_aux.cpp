/* == Estrutura == */
struct aposta {
	vector <int> m_casas;
	vector <int> m_estrelas;
};
struct apostas_jogador {
	int m_id;
	vector <aposta> m_apostas;
};

aposta make_aposta(vector<int> casas, vector<int> estrelas) {
    aposta myaposta;
	myaposta.m_casas = casas;
	myaposta.m_estrelas = estrelas;
    return myaposta;
}

aposta stringToAposta(string str)  {
	aposta m_aposta;
	
    vector <string> str_cortada;      
    vector <string> str_cortada_casas;      
    vector <string> str_cortada_estrelas; 

    aposta aposta_invalida;

    for(int i = 0; i < 5; i++)
		aposta_invalida.m_casas.push_back(0);
	for(int i = 0; i < 2; i++)
		aposta_invalida.m_estrelas.push_back(0);

    str_cortada = explode(" - ", str);


    if (str_cortada.size() == 2) {

	   	str_cortada_casas = explode(" ", str_cortada[0]);
	    str_cortada_estrelas = explode(" ", str_cortada[1]);

	    for(int i = 0; i < str_cortada_casas.size(); i++) {
	    	if(str_cortada_casas[i] == "") {
	    		str_cortada_casas.erase(str_cortada_casas.begin()+i);
	    		i--;
	    	}
	    }

	   	for(int i = 0; i < str_cortada_estrelas.size(); i++) {
	    	if(str_cortada_estrelas[i] == "") {
	    		str_cortada_estrelas.erase(str_cortada_estrelas.begin()+i);
	    		i--;
	    	}
	    }

   		if (str_cortada_casas.size() == 5 && str_cortada_estrelas.size() == 2) {

	    	for(int i = 0; i < 5; i++) {
	    		if(isInteger(str_cortada_casas[i]))
    	       		m_aposta.m_casas.push_back(atoi(str_cortada_casas[i].c_str()));
    	    }
    	    for(int i = 0; i < 2; i++) {
	    		if(isInteger(str_cortada_estrelas[i]))
    	        	m_aposta.m_estrelas.push_back(atoi(str_cortada_estrelas[i].c_str()));
    	    }
    	    sort(m_aposta.m_casas.begin(),m_aposta.m_casas.end());
    	    sort(m_aposta.m_estrelas.begin(),m_aposta.m_estrelas.end());
    	    return m_aposta;

    	} else {
    		return aposta_invalida;
    	}

    } else {
    	return aposta_invalida;
    }

}

/* == Variaveis == */
apostas_jogador novas_apostas;

/* == FORMS == */
string aposta_form(aposta myaposta) {
	string m_aposta;
	for(int i = 0; i < myaposta.m_casas.size(); i++) {
		m_aposta += casa_form(myaposta.m_casas[i]);
		m_aposta += ' ';
	}
	m_aposta += "-";
	for(unsigned int i = 0; i < myaposta.m_estrelas.size(); i++) {
		m_aposta += ' ';
		m_aposta += casa_form(myaposta.m_estrelas[i]);
	}
	return m_aposta;
}

/* Função de verificação da aposta */
bool verify_aposta (const aposta &myaposta) {

	if (myaposta.m_casas.size() == 5 && myaposta.m_estrelas.size() == 2) {
		

		int valor_anterior = 0;
		for (int i = 0; i < 5; i++) {
			if(valor_anterior == myaposta.m_casas[i] || myaposta.m_casas[i] > 50 || myaposta.m_casas[i] < 1){
				return false;
			}
			valor_anterior =  myaposta.m_casas[i];
		}

		valor_anterior = 0;
		for (int i = 0; i < 2; i++) {
			if(valor_anterior == myaposta.m_estrelas[i] || myaposta.m_estrelas[i] > 11 || myaposta.m_estrelas[i] < 1){
				return false;
			}
			valor_anterior =  myaposta.m_estrelas[i];
		}

		return true;
	} else {
		return false;
	}
}

int count_bets() {
//Leitura do bets.txt - Contagem de apostas
	ifstream bets_read ("bets.txt");

	string line_buffer;
	int bets_count = 0;

	while(!bets_read.eof()) {
		getline(bets_read,line_buffer);
		if(line_buffer.size()>ID_LENGTH){
			bets_count++;
		}
	}

	bets_read.close();

	return bets_count;
}

/* == Operações de Leitura e Escrita == */
void writeBets () {
	if(novas_apostas.m_apostas.size() > 0) {
		
		//Escrita no bets.txt - Append
		ofstream bets ("bets.txt", ios::app);
		
		bets << id_form(novas_apostas.m_id) << endl;
		for(int i = 0; i < novas_apostas.m_apostas.size(); i++) {
			bets << aposta_form(novas_apostas.m_apostas[i]) << endl;
		}
		
		bets << endl;
		
		cout <<  novas_apostas.m_apostas.size() << " registadas." << endl;
		
		bets.close();

		

		//Escrita no prize_key.txt
		ofstream prize_key ("prize_key.txt", ios::trunc);

		prize_key << "Montante total de apostas: " << count_bets();

		prize_key.close();

		
	} else {
		cout << "0 apostas registadas." << endl;
	}
}

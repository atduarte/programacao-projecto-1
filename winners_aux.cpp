/* == Estrutura == */
struct premio {
	unsigned int m_premio;
	unsigned int m_acertos_casas;
	unsigned int m_acertos_estrelas;
	unsigned int m_numvenc;
	double m_valor;
};

struct info_aposta_vencedora {
	string m_aposta_com_asterisco;
	int m_acertos_casas;
	int m_acertos_estrelas;
	double m_premio;
};

struct info_jogador_vencedor {
	int m_id;
	string m_nome;
	double m_premio_total;
	vector <aposta> m_apostas;
	vector <info_aposta_vencedora> m_apostas_ganhas;
};

info_jogador_vencedor make_info_jogador_vencedor(int fm_id, string fm_nome) {
	info_jogador_vencedor my_info_jogador_vencedor;

	my_info_jogador_vencedor.m_id = fm_id;
	my_info_jogador_vencedor.m_nome = fm_nome;

	return my_info_jogador_vencedor;
}

/* == Variáveis == */

vector <premio> premios (13);

vector <info_jogador_vencedor> jogadores_winners;

unsigned int n_total_apostas = 0;

/* == Funções == */
void reset_premios() 
{
	for(int i = 0; i < 13; i++) 
	{

		premios[i].m_premio = i+1;

		//Definir casas
		if(i <= 2 ) {
			premios[i].m_acertos_casas = 5;
		} else if(i <= 5) {
			premios[i].m_acertos_casas = 4;
		} else if(i == 6 || i == 8 || i == 9) {
			premios[i].m_acertos_casas = 3;
		} else if(i == 7 || i == 11 || i == 12) {
			premios[i].m_acertos_casas = 2;
		} else if(i == 10) {
			premios[i].m_acertos_casas = 1;
		}

		if(i == 0 || i == 3 || i == 6 || i == 7 || i == 10) {
			premios[i].m_acertos_estrelas = 2;
		} else if(i == 1 || i == 4 || i == 8 || i == 11) {
			premios[i].m_acertos_estrelas = 1;
		} else if(i == 2 || i == 5 || i == 9 || i == 12) {
			premios[i].m_acertos_estrelas = 0;
		} 

		premios[i].m_numvenc = 0;
		premios[i].m_valor = 0;

	}
}


void readBets()
{
	ifstream bets("bets.txt");

	if(bets) 
	{
		string line_buffer;
		int pos, id;

		while(!bets.eof()) 
		{
			getline(bets, line_buffer);
			if(line_buffer.size()==ID_LENGTH)
			{
				line_buffer = line_buffer.substr(0, ID_LENGTH);
				id = atoi(line_buffer.c_str());
				for (int i = 0; i < jogadores_winners.size(); i++) {
					if (jogadores_winners[i].m_id == id) {
						pos = i;
						break;
					}
				}
			}
			else if (line_buffer.size() > 5)
			{
				line_buffer = line_buffer.substr(0,22);
				jogadores_winners[pos].m_apostas.push_back(stringToAposta(line_buffer));
				n_total_apostas++;
			}
		}
	}

	bets.close();
}

int devolve_n_premio(int n_casas, int n_estrelas) {

			switch(n_casas) {
				case 5: if(n_estrelas == 2) { return 1; }
						if(n_estrelas == 1) { return 2; }
				        if(n_estrelas == 0) { return 3; }
						break;
				case 4: if(n_estrelas == 2) { return 4; }
				        if(n_estrelas == 1) { return 5; }
				        if(n_estrelas == 0) { return 6; }
						break;
				case 3: if(n_estrelas == 2) { return 7; }
				        if(n_estrelas == 1) { return 9; }
				        if(n_estrelas == 0) { return 10; }
						break;
				case 2: if(n_estrelas == 2) { return 8;}
				        if(n_estrelas == 1) { return 12; }
				        if(n_estrelas == 0) { return 13; }
						break;
				case 1: if(n_estrelas == 2) { return 11; }
						return 0;
						break;
				default: return 0;
						 break;
			}

			return 0;
}

void writeWinnersPrize_key() {
	ifstream winners_prize_key_read ("prize_key.txt");

	string dummy, prize_key_beginning = "";

	if(winners_prize_key_read) {
		for(int i = 0; i < 2; i++){		
			getline(winners_prize_key_read, dummy);
			prize_key_beginning += dummy;
			prize_key_beginning += "\n";
		}
	}

	winners_prize_key_read.close();

	ofstream winners_prize_key ("prize_key.txt");

	winners_prize_key << prize_key_beginning;

	winners_prize_key << "Premio Acertos Num.Venc.      Valor     " << endl;
	winners_prize_key << "------ ------- --------- ---------------" << endl;

	for (int i = 0; i < premios.size(); i++) {

		winners_prize_key << "  " << setw(2) << right << setfill(' ') << i+1 << "  " << " "
						  << "  " << premios[i].m_acertos_casas << "+" << premios[i].m_acertos_estrelas << "  " << " "
						  << setw(9) << premios[i].m_numvenc << " "
						  << setw(15) << setprecision(2) << fixed << premios[i].m_valor << endl;
	}

	winners_prize_key.close();
}

void writeWinners() {

	ofstream winners ("winners.txt");

	for (int i = 0; i < jogadores_winners.size(); ++i)
	{
		winners << id_form(jogadores_winners[i].m_id) << " - " 
				<< setw(NOME_LENGTH) << setfill(' ') << right << jogadores_winners[i].m_nome << " - "
				<< setw(5) << jogadores_winners[i].m_apostas.size() << "/"
				<< setw(1) << jogadores_winners[i].m_apostas_ganhas.size() << " - "
				<< setw(5) << setprecision(2) << fixed << jogadores_winners[i].m_premio_total << endl;

		for (int j = 0; j < jogadores_winners[i].m_apostas_ganhas.size(); j++)
		{
			winners << jogadores_winners[i].m_apostas_ganhas[j].m_aposta_com_asterisco
					<< " (" << jogadores_winners[i].m_apostas_ganhas[j].m_acertos_casas
					<< "+" << jogadores_winners[i].m_apostas_ganhas[j].m_acertos_estrelas
					<< ")=" << setw(10) << left << setfill(' ') << setprecision(2) << fixed << jogadores_winners[i].m_apostas_ganhas[j].m_premio
					<< endl;
		}

		winners << endl;

	}

	winners.close();

}

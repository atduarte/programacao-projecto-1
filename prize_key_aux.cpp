/* == Vari�veis == */
aposta main_kf;

/* == Fun��es de Ler e Escrever == */
void readPrize_key () {
	
	ifstream prize_key ("prize_key.txt");
	
	if(prize_key) {
		string line_buffer;
		getline(prize_key, line_buffer);
		getline(prize_key, line_buffer);
		main_kf = stringToAposta(line_buffer.substr(19, APOSTA_LENGTH));
	}
	
	prize_key.close();
}

void generate_writePrize_key () {
	
	ifstream prize_key ("prize_key.txt");
	
	string line_buffer;
	
	if(prize_key) {		
		getline(prize_key, line_buffer);
	}
	
	prize_key.close();
	
	ofstream prize_key_tmp ("prize_key_tmp.txt", ios::trunc);
	
	prize_key_tmp << line_buffer << endl;
	prize_key_tmp << "Chave do concurso: " << aposta_form(make_aposta(randomize(1,50,5),randomize(1,11,2))) << endl;

	prize_key_tmp.close();
	
	//Remover o ficheiro prize_key.txt
	ifstream prize_key_to_delete ("prize_key.txt");
	if(prize_key_to_delete) {
		prize_key_to_delete.close();
		if( remove( "prize_key.txt" ) != 0 ) 
			cerr << "Error - deleting file" << endl;
	}
	
	
	//Renomear o ficheiro tempor�rio para prize_key.txt
	int result = rename( "prize_key_tmp.txt" , "prize_key.txt" );
    if ( result != 0 )
		cerr << "Error renaming file" << endl;

}


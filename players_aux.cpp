/* == Estrutura == */
struct jogador {
	int m_id;
	string m_nome;
	double m_saldo;
};

jogador make_jogador(int id, string nome, double saldo) {
    jogador myjogador;
	myjogador.m_id = id;
	myjogador.m_nome = nome;
	myjogador.m_saldo = saldo;
    return myjogador;
}

/* == Vari�veis == */
vector <jogador> jogadores;
unsigned int id_jogador = 0;

/* == Opera��es de Leitura e Escrita == */
void readPlayers() {
	ifstream players("players.txt");
	
	if(players) {
		jogador jogador_buffer;
		string line_buffer, nome_buffer;
		int id_buffer;
		double saldo_buffer;
		
		//Buscar ID da 1� linha do ficheiro
		getline(players, line_buffer);
		id_jogador = atoi(line_buffer.substr(0,ID_LENGTH).c_str());
			
		while ( !players.eof() ) {
			getline(players, line_buffer);
			if(line_buffer.size() > 5) {
				//Separar elementos da linha e converter
				id_buffer = atoi(line_buffer.substr(0,ID_LENGTH).c_str());
				nome_buffer = line_buffer.substr(ID_LENGTH + 1, NOME_LENGTH);
				saldo_buffer = atof(line_buffer.substr(ID_LENGTH + 1 + NOME_LENGTH + 1,SALDO_LENGTH).c_str());
				//Acrescentar elementos ao vector jogadores
				jogadores.push_back(make_jogador(id_buffer, nome_buffer, saldo_buffer));
			}
		}
	}
	
	players.close();
}

void writePlayers () {
	ofstream players_tmp ("players_tmp.txt");
	
	players_tmp << id_form(id_jogador);

	for(unsigned int i = 0; i < jogadores.size(); i++)
	{
		players_tmp << endl 
					<< id_form(jogadores[i].m_id) << " " 
					<< nome_form(jogadores[i].m_nome) << " " 
					<< setw(SALDO_LENGTH) << setfill(' ') << right << setprecision(2) << fixed << jogadores[i].m_saldo;
	}
	
	players_tmp.close();
	
	//Remover o ficheiro players.txt
	ifstream players("players.txt");
	if(players) {
		players.close();
		if( remove( "players.txt" ) != 0 ) 
			cerr << "Error - deleting file" << endl;
	}
	
	
	//Renomear o ficheiro tempor�rio para players.txt
	int result = rename( "players_tmp.txt" , "players.txt" );
    if ( result != 0 )
		cerr << "Error renaming file" << endl;

}

#include <fstream>
#include <iostream>

using namespace std;

#include "main_aux.cpp"
#include "players_aux.cpp"

string buffer;
bool repeat = false;

/* === FUNÇÕES === */
void registar() {
	system("cls");
	cout << "== Registar Jogador == " << endl << endl;
	
	//Ler dados
	string nome, saldo;
	cout << "Insira o nome: ";
	getline(cin, nome);

	if(nome.size() > NOME_LENGTH) {
		cout << "AVISO: O Nome tem um maximo de 20 caracteres, portanto sera' truncado." << endl;
		nome = nome.substr(0, NOME_LENGTH);
	}

	do {
		cout << "Insira o saldo: ";
		getline(cin, saldo);

		if(isFloat(saldo)) {
			repeat = false;
			if (atof(saldo.c_str()) > 9999999999.99) {
				cout << "Valor introduzido e' demasiado grande. Introduza novamente, por favor." << endl;
				repeat = true;
			}
			if (atof(saldo.c_str()) < 0) {
				cout << "Valor introduzido nao pode ser negativo. Introduza novamente, por favor." << endl;
				repeat = true;
			}
		} else {
			repeat = true;
			cout << "Valor introduzido nao e' um numero. Introduza novamente, por favor." << endl;
		}

	} while (repeat);

	id_jogador++;
	jogadores.push_back(make_jogador(id_jogador, nome, atof(saldo.c_str())));
	
	writePlayers();
	
	system("pause");
}

void eliminar() {
	system("cls");
	cout << "== Eliminar Jogador == " << endl << endl;
	
	int id;
	
	do {
		//Ler dados
		cout << "Insira o ID do jogador que pretende remover: ";
		getline(cin,buffer);

		//Verificar Dados
		if(isInteger(buffer)) {
			repeat = false;
			id = atoi (buffer.c_str());
		} else {
			repeat = true;
			cout << "Valor introduzido nao e' um numero. Introduza novamente, por favor." << endl;
		}
		
	} while(repeat);
	
	bool user_found = false;
	
	//Procurar no vector jogadores e eliminar
	for(int i = 0; i < jogadores.size(); i++) {
		if(jogadores[i].m_id == id) {
			user_found = true;
			jogadores.erase(jogadores.begin()+i);
			break;
		}
	}
	
	if(user_found)
		writePlayers();
	else
		cerr << "Jogador nao encontrado." << endl;

	
	system("pause");
}

void consultar() {
	system("cls");
	cout << "== Consultar Saldo == " << endl << endl;
	
	//Ler dados
	int id;

	do {
		//Ler dados
		cout << "Insira o ID do jogador: ";
		getline(cin,buffer);

		//Verificar Dados
		if(isInteger(buffer)) {
			repeat = false;
			id = atoi (buffer.c_str());
		} else {
			repeat = true;
			cout << "Valor introduzido nao e' um numero. Introduza novamente, por favor." << endl;
		}
		
	} while(repeat);
	
	bool user_found = false;
	
	//Procurar no vector jogadores e eliminar
	for(int i = 0; i < jogadores.size(); i++) {
		if(jogadores[i].m_id == id) {
			user_found = true;
			cout << "Saldo: " << jogadores[i].m_saldo << endl;
			system("pause");
			break;
		}
	}
	
	if(!user_found) {
		cerr << "Jogador nao encontrado." << endl;
		system("pause");
	}
}

void carregar() {
	system("cls");
	cout << "== Carregar Saldo == " << endl << endl;
	
	//Ler dados
	int id, user_i;
	double saldo_to_add, novo_saldo;

	//Ler ID
	do {
		cout << "Insira o ID do jogador: ";
		getline(cin,buffer);

		//.. verificar Dados
		if(isInteger(buffer)) {
			repeat = false;
			id = atoi (buffer.c_str());
		} else {
			repeat = true;
			cout << "Valor introduzido nao e' um numero. Introduza novamente, por favor." << endl;
		}
		
	} while(repeat);

	bool user_found = false;
	
	//Procurar no vector jogadores e eliminar
	for(int i = 0; i < jogadores.size(); i++) {
		if(jogadores[i].m_id == id) {
			user_found = true;
			user_i = i;
			break;
		}
	}

	if(!user_found) {
		cerr << "Jogador nao encontrado." << endl << endl;
	} else {
		cerr << "Jogador encontrado." << endl << endl;
		//Ler Saldo
		do {
			cout << "Saldo a carregar: ";
			getline(cin,buffer);

			//.. verificar Dados
			if(isFloat(buffer)) {
				repeat = false;
				if(atof (buffer.c_str()) < 0) {
					repeat = true;
					cout << "Valor introduzido nao pode ser negativo. Introduza novamente, por favor." << endl;
				}
				saldo_to_add = atof (buffer.c_str());
				novo_saldo = jogadores[user_i].m_saldo + saldo_to_add;
				if(novo_saldo > 9999999999.99) {
					repeat = true;
					cout << "Valor do saldo nao pode ser tao alto. Introduza novamente, por favor." << endl;
				}
			} else {
				repeat = true;
				cout << "Valor introduzido nao e' um numero. Introduza novamente, por favor." << endl;
			}
			
		} while(repeat);

		jogadores[user_i].m_saldo += saldo_to_add;
		cout << "Novo saldo: " << jogadores[user_i].m_saldo << endl;

		writePlayers();
	}
		
	system("pause");
}

int main() {
	const char OP_NUMBER = '5';
	
	readPlayers();

	char op = '0'; //operation

	do {
		system("cls");
		cout << "== Programa de Gestao de Jogadores == " << endl << endl;
		cout << "1 - Registar Jogador" << endl;
		cout << "2 - Eliminar Jogador" << endl;
		cout << "3 - Consultar Saldo do Cartao do Jogador" << endl;
		cout << "4 - Carregar Saldo do Cartao do Jogador" << endl;
		cout << "5 - Sair" << endl;
		getline(cin,buffer);

		op = buffer[0];

		switch(op) {
			case '1':	registar();
					break;
			case '2': eliminar();
					break;
			case '3':	consultar();
					break;
			case '4': carregar();
					break;
		}
	} while (op!=OP_NUMBER);
	

	return 0;
}